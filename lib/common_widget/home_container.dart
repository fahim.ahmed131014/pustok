import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pustok/utils/constants.dart';

class HomeContainer extends StatelessWidget {
  final IconData? icon;
  final String? containerText;
  final Function()? onPressed;

  HomeContainer({@required this.icon, @required this.containerText,@required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: const EdgeInsets.only(left:14,right:14),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.lightBlue.shade700,
        ),
        child: Column(
          mainAxisAlignment: kColumnMainAxis,
          children: [
            const SizedBox(
              height:kHomePageSizedBoxHeight,
            ),
            Icon(icon!,
              size: kHomePageIconSize,),
            const SizedBox(
              height:kHomePageSizedBoxHeight,
            ),
            Text(containerText!,textAlign:TextAlign.center,style:kHomePageContainerTextStyle),
            const SizedBox(
              height:kHomePageSizedBoxHeight,
            ),
          ],
        ),

      ),
    );
  }
}