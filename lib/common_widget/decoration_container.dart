import 'package:flutter/material.dart';
import 'package:pustok/utils/constants.dart';

class DecorationContainer extends StatelessWidget {
  final Widget? decorationChild;
  final String? imageLocation;
  final Color? backgroundColor;
  final double? imageOpacity;

  DecorationContainer({@required this.decorationChild, @required this.imageLocation, @required this.backgroundColor, @required this.imageOpacity});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
              imageLocation!,
          ),
          colorFilter: ColorFilter.mode(
              backgroundColor!.withOpacity(imageOpacity!), BlendMode.dstATop),
          fit: BoxFit.cover,
        ),
      ),
      child: decorationChild,
    );
  }
}
