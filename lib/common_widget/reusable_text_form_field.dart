import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class ReusableTextFormField extends StatelessWidget {
  final String? hintText;
  final TextInputType? keyBoardType;
  final TextEditingController? textEditingController;
  final String? validText;
  final bool? isName;




  ReusableTextFormField({@required this.hintText, @required this.keyBoardType, @required this.textEditingController, @required this.validText, @required this.isName});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width:300,
      child: TextFormField(
        keyboardType: keyBoardType,
        controller: textEditingController,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0,horizontal: 10),
            hintText: hintText,
            hintStyle: TextStyle(
              fontFamily: "acme",
            ),
            errorStyle: const TextStyle(fontSize: 11, height: 0.3),
            errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                width: .5,
                color: Colors.red,
              ),
            ),

            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedErrorBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color: Colors.red,
              ),
            ),

            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey.shade400)
            ),
        ),
        style: const TextStyle(
          fontSize: 16,
          color: Colors.white,
          fontFamily: "acme",
        ),
        validator: (value) {
          var email = textEditingController!.text;
          bool isValid = EmailValidator.validate(email);
          if(isName == true) {

          }else {
            if (value == "" || value == null || isValid == false) {
              return validText;
            }
            else {
              return null;
            }

          }

        },
      ),

    );
  }
}