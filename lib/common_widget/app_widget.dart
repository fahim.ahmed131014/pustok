import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pustok/pages/add_cart.dart';
import 'package:pustok/utils/constants.dart';

class AppWidget extends StatelessWidget implements PreferredSizeWidget{
  final IconData? leadingIcon;
  final String? pageTitle;
  final Function()? onPressed;



  @override
  Size get preferredSize => Size.fromHeight(50);

  AppWidget(
      {@required this.leadingIcon, @required this.pageTitle, @required this.onPressed});

  @override
  Widget build(BuildContext context) {

    return AppBar(
        leading: InkWell(
          onTap: onPressed,
          child: Icon(leadingIcon,
              color: kAppBarColor),
        ),
        title: Text(pageTitle!, style: kAppBarTextStyle),
        centerTitle: true,
        backgroundColor: kBlackColor,
        actions: [
            IconButton(
            onPressed: () {
               Navigator.push(context, MaterialPageRoute(builder: (context){
                 return AddCart();
               }));
            },
              color:kBlackColor,
              icon: Icon(FontAwesomeIcons.shoppingCart,
              color: kAppBarColor,),
          ),
        ],


    );


  }
}



