import 'package:flutter/material.dart';
import 'package:pustok/utils/constants.dart';

class CategoryContainer extends StatelessWidget {
  final String? containerText;
  final Function()? onPressed;
  CategoryContainer({@required this.containerText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.all(kMarginValue),
        height: kCategoryContainerHeight,
        decoration: BoxDecoration(
          color: kTransparentColor,
          borderRadius: BorderRadius.circular(kCategoryContainerBorderRadius),
        ),
        child: Center(
          child: Text(containerText!,
              textAlign: TextAlign.center,
              style: kCategoryPageTextStyle),
        ),
      ),
    );
  }
}