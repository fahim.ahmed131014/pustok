import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';


enum ButtonStatus{
  isEnable,
  isDisable
}

ButtonStatus? currentStatus = ButtonStatus.isDisable;

class ReusablePasswordFormField extends StatefulWidget {
  final String? hintText;
  final TextInputType? keyBoardType;
  final TextEditingController? textEditingController;
  final String? validText;
  final String? validTextForLength;

  ReusablePasswordFormField({@required this.hintText, @required this.keyBoardType, @required this.textEditingController, @required this.validText,  @required this.validTextForLength});

  @override
  State<ReusablePasswordFormField> createState() => _ReusablePasswordFormFieldState();
}

class _ReusablePasswordFormFieldState extends State<ReusablePasswordFormField> {




  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_,authProvider,___) {
        return Container(
          height: 100,
          width:300,
          child: TextFormField(
            keyboardType: widget.keyBoardType,
            controller: widget.textEditingController,
            obscureText: authProvider.isPasswordShowingText,
            onTap: (){

              authProvider.isPasswordShowingText = true;
              authProvider.isPasswordEyeColorEnabled = false;

            },
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: (){
                  if(currentStatus == ButtonStatus.isEnable){
                    authProvider.isPasswordShowingText = false;
                    authProvider.isPasswordEyeColorEnabled = true;
                    currentStatus = ButtonStatus.isDisable;
                  }
                  else{
                    authProvider.isPasswordShowingText = true;
                    authProvider.isPasswordEyeColorEnabled = false;
                    currentStatus = ButtonStatus.isEnable;
                  }

                },
                  child: Icon(FontAwesomeIcons.eye,
                  color: authProvider.isPasswordEyeColorEnabled == false ? Colors.white : Colors.blueAccent
                  ),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0,horizontal: 10),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                fontFamily: "acme",
              ),
              errorStyle: const TextStyle(fontSize: 12, height: 0.4),
              errorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: .5,
                  color: Colors.red,
                ),
              ),

              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedErrorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.red,
                ),
              ),

              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color:Colors.blueAccent),
              )
            ),
            style: const TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontFamily: "acme",
            ),
            validator: (value){
              if(value == "" || value == null) {
                return widget.validText;
              }else if(widget.textEditingController!.text.length <=7){
                return widget.validTextForLength;
              }
              else{
                return null;
              }
            },

          ),

        );
      }
    );
  }
}