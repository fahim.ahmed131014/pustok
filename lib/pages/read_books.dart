import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:pustok/models/book_info.dart';

class ReadBooks extends StatefulWidget {
  final BookInfo? bookInfo;
  ReadBooks({@required this.bookInfo});

  @override
  State<ReadBooks> createState() => _ReadBooksState();
}

class _ReadBooksState extends State<ReadBooks> {



  @override
  Widget build(BuildContext context) {

    final PdfController _pdfViewController = PdfController(
      viewportFraction: 1,
      document: PdfDocument.openAsset(widget.bookInfo!.bookPdfLocation!),
    );

    return Scaffold(
      appBar:AppBar(
        title: Text(widget.bookInfo!.bookName!.toUpperCase()),
        centerTitle: true,
      ),
      body: PdfView(
        controller: _pdfViewController,
        scrollDirection: Axis.vertical,
        documentLoader: CircularProgressIndicator(),
      ),
    );
  }
}
