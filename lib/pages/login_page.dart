import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/common_widget/login_register_button.dart';
import 'package:pustok/common_widget/resuable_password_form_field.dart';
import 'package:pustok/common_widget/reusable_text_form_field.dart';
import 'package:pustok/models/user.dart';
import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pages/registration_page.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';
import 'package:pustok/utils/constants.dart';
import 'package:pustok/utils/custom_toast.dart';
import 'package:pustok/utils/memory_management.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController? emailController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();




  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          body: SafeArea(
            child: DecorationContainer(
              imageLocation: "assets/images/login_background.jpg",
              imageOpacity: 0.1,
              backgroundColor: kBackgroundColor,
              decorationChild: SizedBox(
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(top:250),
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Login".toUpperCase(),
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: "acme",
                          fontSize: 60,
                         ),
                        ),
                        const SizedBox(
                          height:70,
                        ),
                        ReusableTextFormField(
                          hintText: "Enter Your Email",
                          keyBoardType: TextInputType.emailAddress,
                          textEditingController: emailController,
                          validText: "Enter a valid email please",
                        ),
                        ReusablePasswordFormField(
                          hintText: "Enter Your Password",
                          keyBoardType: TextInputType.text,
                          textEditingController: passwordController,
                          validText: "Enter a valid password",
                          validTextForLength: "Enter at least 8 characters",
                        ),
                        LoginRegisterButton(
                          buttonName: "LogIn".toUpperCase(),
                          onPressed: ()async {
                              if(formKey.currentState!.validate()){

                                User user = User(
                                  email: emailController!.text,
                                  password: passwordController!.text
                                );

                                bool status = await provider.loginUser(user);
                                if(status == true){
                                  CustomToast.toastShower("Login Successfully Completed", Colors.green);

                                  MemoryManagement.setLoggedIn(isLogged: true);
                                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)
                                  => HomePage()),
                                          (route) =>false);
                                }
                                else{
                                  CustomToast.toastShower("Login Failed, check email or password", Colors.red);
                                }
                              }
                          },
                        ),
                        SizedBox(
                          height:40
                        ),
                        Wrap(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom:100.0),
                              child: Text(
                                "Create A New Account,",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: "acme"
                                ),
                              ),
                            ),
                            InkWell(
                              onTap:(){
                                  Navigator.push(context, MaterialPageRoute(builder: (context){
                                    return RegistrationPage();
                                  })
                                  );
                              },
                              child: Text("SignUp".toUpperCase(),
                                style: TextStyle(
                                  fontFamily: "acme",
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }
    );
  }
}




