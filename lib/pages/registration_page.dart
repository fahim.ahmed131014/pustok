import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/common_widget/login_register_button.dart';
import 'package:pustok/common_widget/resuable_password_form_field.dart';
import 'package:pustok/common_widget/reusable_text_form_field.dart';
import 'package:pustok/models/user.dart';
import 'package:pustok/pages/login_page.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';
import 'package:pustok/utils/constants.dart';
import 'package:pustok/utils/custom_toast.dart';


class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {

  final formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();






  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          body: DecorationContainer(
            imageLocation: "assets/images/registration_page_background.png",
            imageOpacity: 0.1,
            backgroundColor: kBackgroundColor,
            decorationChild: SizedBox(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(top:100),
                child: Form(
                  key: formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("SignUp".toUpperCase(),
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: "acme",
                          fontSize: 60,
                        ),
                      ),
                      const SizedBox(
                        height:70,
                      ),
                      ReusableTextFormField(
                        hintText: "Enter Your Name",
                        keyBoardType: TextInputType.emailAddress,
                        textEditingController: nameController,
                        validText: "Enter a valid name please",
                        isName: true,
                      ),
                      ReusableTextFormField(
                        hintText: "Enter Your Email",
                        keyBoardType: TextInputType.emailAddress,
                        textEditingController: emailController,
                        validText: "Enter a valid email please",
                        isName: false,
                      ),
                      ReusablePasswordFormField(
                        hintText: "Enter Your Password",
                        keyBoardType: TextInputType.text,
                        textEditingController: passwordController,
                        validText: "Enter a valid password",
                        validTextForLength: "Enter at least 8 characters",
                      ),

                      ReusablePasswordFormField(
                        hintText: "Confirm Password",
                        keyBoardType: TextInputType.text,
                        textEditingController: confirmPasswordController,
                        validText: "Enter a valid password",
                        validTextForLength: "Enter at least 8 characters",
                      ),

                      LoginRegisterButton(
                        buttonName: "SignUp".toUpperCase(),
                        onPressed: () async{
                          if(formKey.currentState!.validate()) {
                            if(passwordController.text == confirmPasswordController.text){
                              User user = User(
                                name: nameController.text,
                                email: emailController.text,
                                password: passwordController.text,
                              );

                              bool status = await provider.isUserExist(emailController.text);
                              if(status == true){
                                CustomToast.toastShower("User is already existed", Colors.redAccent);
                              }
                              else{
                                 bool status = await provider.saveUser(user);
                                 if(status == true){
                                   CustomToast.toastShower("Registration Complete", Colors.green);
                                   Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)
                                       => LoginPage()),
                                       (route) =>false);
                                 }else{
                                   CustomToast.toastShower("Registration Failed", Colors.red);
                                 }
                              }

                            }
                            else{
                               CustomToast.toastShower("Passwords are not matched", Colors.redAccent);
                            }

                          }
                        },
                      ),
                      SizedBox(
                          height:40
                      ),
                      Wrap(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom:100.0),
                            child: Text(
                              "Already has an account?",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: "acme"
                              ),
                            ),
                          ),
                          InkWell(
                            onTap:(){
                              Navigator.push(context, MaterialPageRoute(builder: (context){
                                return LoginPage();
                              })
                              );
                            },
                            child: Text("LogIn".toUpperCase(),
                              style: TextStyle(
                                fontFamily: "acme",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        );
      }
    );
  }
}
