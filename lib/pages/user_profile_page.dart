import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';
import 'package:pustok/utils/constants.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({Key? key}) : super(key: key);

  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          appBar: AppWidget(
            pageTitle: "Profile".toUpperCase(),
            leadingIcon: FontAwesomeIcons.backward,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          body: DecorationContainer(
            imageLocation: "assets/images/profile_background.jpg",
            imageOpacity: 0.1,
            backgroundColor: kBackgroundColor,
            decorationChild: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:20.0,right:50),
                      child: Text("Username".toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 30,
                        fontFamily: "acme"
                      ),),
                    ),
                    SizedBox(
                      width:30,
                    ),
                    Text(provider.users[0].name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          fontFamily: "acme",

                      ),),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Row(

                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:20.0,right:50),
                      child: Text("Email".toUpperCase(),
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 30,
                            fontFamily: "acme"
                        ),),
                    ),
                    SizedBox(
                      width:30,
                    ),
                    Text(provider.users[0].email,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                        fontFamily: "acme",

                      ),),
                  ],
                ),
              ],


            ),
          ),
        );
      }
    );
  }
}
