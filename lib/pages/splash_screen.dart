import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pages/welcome_page.dart';
import 'package:pustok/utils/memory_management.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SplashScreen  extends StatefulWidget {

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  Duration duration = Duration(seconds:5);


  @override
  void initState() {
    super.initState();
    initSharedPreferences();
  }


  void initSharedPreferences()async{
    await MemoryManagement.initSharedPreferences();
    Timer(duration,
            () {

      try{
        if(MemoryManagement.getLoggedIn() == true){
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return HomePage();
          })
          );
        }else{
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return WelcomePage();
          })
          );
        }
      }catch(error){
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return WelcomePage();
        })
        );
      }

        });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
       body: SafeArea(
        child: DecorationContainer(
          imageLocation: "assets/images/splash_screen_background.jpg",
          imageOpacity: 0.1,
          backgroundColor: Colors.black,
          decorationChild: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex:2,
                child:Container(),
              ),
              Container(
                height: 250,
                width: 250,
                child: Image.asset(
                  "assets/icons/app_logo.png",
                ),
              ),
              Expanded(
                flex:1,
                child: Container(),
              ),
              Container(
                height:2,
                width:250,
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  //color: Colors.green,
                  backgroundColor: Colors.transparent,

                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }
}

