
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';
import '../common_widget/decoration_container.dart';
import '../models/book_list.dart';

class TopChart extends StatefulWidget {
  const TopChart({Key? key}) : super(key: key);

  @override
  _TopChartState createState() => _TopChartState();
}

class _TopChartState extends State<TopChart> {


 @override
  void initState() {
    super.initState();
 }






  @override
  Widget build(BuildContext context) {
   return Consumer<PustokProvider>(
     builder: (_,provider,__) {
       return Scaffold(
          appBar: AppWidget(
            leadingIcon : FontAwesomeIcons.backward,
            pageTitle: "Top Chart".toUpperCase(),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          body: SafeArea(
            child: DecorationContainer(
              imageLocation: "assets/images/top_chart_background.jpg",
              backgroundColor: kBackgroundColor,
              imageOpacity: 0.2,
              decorationChild:  SingleChildScrollView(
                child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        SizedBox(
                          height: 20,
                        ),

                        ListView.separated(
                            itemCount: provider.bookList.topChartBookStoreList.length,
                            separatorBuilder: (context,index){
                              return SizedBox(
                                height: 50,
                              );
                            },
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                            return Container(
                              height: kBoxContainerHeight,
                              width: kBoxContainerWidth,
                              color: kTransparentColor,
                              child:Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Image.asset(provider.bookList.topChartBookStoreList[index].imageLocation!),
                                  Column(
                                    mainAxisAlignment: kColumnMainAxis,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(
                                            provider.bookList.topChartBookStoreList[index].bookName!.toUpperCase(),style: kBookNameTextStyle),
                                      ),
                                      SizedBox(
                                        height: kBookContainerSizedBoxHeight,
                                      ),
                                      Text(
                                          provider.bookList.topChartBookStoreList[index].bookWriter!.toUpperCase(),style: kBookWriterTextStyle),
                                      SizedBox(
                                        height: 36,
                                      ),
                                      Text(
                                        provider.bookList.topChartBookStoreList[index].bookGenre!.toUpperCase(),style: kBookGenreTextStyle,
                                      ),
                                      SizedBox(
                                        height: kBookContainerSizedBoxHeight2,
                                      ),
                                      Text(
                                          provider.bookList.topChartBookStoreList[index].bookPublished!.toUpperCase(), style: kBookGenreTextStyle),
                                      SizedBox(
                                        height: kBookContainerSizedBoxHeight2,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              color: kAppBarColor,
                                              borderRadius: BorderRadius.circular(kButtonMarginValue),
                                            ),
                                            padding: const EdgeInsets.all(kBookButtonPaddingValue),
                                            child: InkWell(
                                              onTap: (){
                                                provider.favouriteBookList.add(provider.bookList.topChartBookStoreList[index]);
                                              },
                                              child: Text(
                                                "Favourites".toUpperCase(),
                                                style: kBookButtonTextStyle,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: kBoxButtonSizedBoxValue,
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                              color: kAppBarColor,
                                              borderRadius: BorderRadius.circular(kButtonMarginValue),
                                            ),
                                            padding: const EdgeInsets.all(kBookButtonPaddingValue),
                                            child: InkWell(
                                              onTap: (){
                                                provider.addShopList.add(provider.bookList.topChartBookStoreList[index]);
                                                },
                                              child: Text(
                                                "Add To Shop".toUpperCase(),
                                                style: kBookButtonTextStyle,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),

                                ],
                              ),
                            );
                          }
                        ),
                      ],
                    ),
              ),


              ),
              ),

            );
     }
   );



  }
}
