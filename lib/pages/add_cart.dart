import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pages/confirm_shopping.dart';
import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';

class AddCart extends StatefulWidget {


  @override
  _AddCartState createState() => _AddCartState();
}

class _AddCartState extends State<AddCart> {



  @override
  Widget build(BuildContext context) {
    return Consumer<PustokProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          appBar: AppWidget(
            leadingIcon : FontAwesomeIcons.backward,
            pageTitle: "Add To Cart".toUpperCase(),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return HomePage();
                })
              );
              },
          ),
          body: SafeArea(
            child: DecorationContainer(
              imageLocation: "assets/images/add_to_cart_background.jpg",
              backgroundColor: kBackgroundColor,
              imageOpacity: 0.2,
              decorationChild:  SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 20,
                    ),

                    provider.addShopList.isEmpty? Padding(
                      padding: const EdgeInsets.only(top: 300.0),
                      child: Center(
                          child: Text(
                              "Nothing to Buy, Add Some Books First",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.lightBlueAccent,
                              fontFamily: "acme"
                            ),
                          )
                      ),
                    ):
                    ListView.separated(
                        itemCount: provider.addShopList.length,
                        separatorBuilder: (context,index){
                          return SizedBox(
                            height: 10,
                          );
                        },
                        shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Container(
                                height: 300,
                                width: kBoxContainerWidth,
                                color: kTransparentColor,
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(provider.addShopList[index].imageLocation!,
                                      height: 150,
                                      width: 100,
                                      ),
                                      SizedBox(
                                        height: kBookContainerSizedBoxHeight,
                                      ),
                                      Text(provider.addShopList[index].bookName!.toUpperCase(),
                                          style: kBookNameTextStyle),
                                      SizedBox(
                                        height: kBookContainerSizedBoxHeight,
                                      ),
                                      Text(provider.addShopList[index].bookWriter!.toUpperCase(),
                                          style: kBookWriterTextStyle),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      ElevatedButton(
                                          onPressed: (){
                                            provider.confirmedBuyBookList.add(provider.addShopList[index]);
                                            Navigator.push(context, MaterialPageRoute(builder: (context){
                                                 return ConfirmShopping();
                                              })
                                            );
                                          },
                                          child: Text("Purchase".toUpperCase(),
                                            style:TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontFamily: "acme",
                                            )
                                          ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                  ],
                ),
              ),


            ),
          ),

        );
      }
    );
  }
}
