import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/category_container.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pages/action_adventure_page.dart';
import 'package:pustok/pages/detective_mystery_page.dart';
import 'package:pustok/utils/constants.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         appBar: AppWidget(
           pageTitle: "Category".toUpperCase(),
           leadingIcon: FontAwesomeIcons.backward,
           onPressed: (){
             setState(() {
               Navigator.pop(context);
             });
           },
         ),
      body: SafeArea(
        child: DecorationContainer(
          imageLocation: "assets/images/category_background.jpg",
          backgroundColor: kBackgroundColor,
          imageOpacity: kImageOpacity,
          decorationChild: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: kColumnMainAxis,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: kHomePageSizedBoxHeight,
                ),
                CategoryContainer(
                  onPressed: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context){
                       return ActionAdventurePage();
                     }),
                     );
                  },
                  containerText: "Action   &   Adventure".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){

                  },
                  containerText: "Classic".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return DetectiveMysteryPage();
                    }),
                    );

                  },
                  containerText: "Detective   &   Mystery".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){


                  },
                  containerText: "Fantasy".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){

                  },
                  containerText: "Horror".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){

                  },
                  containerText: "Romance".toUpperCase(),
                ),
                CategoryContainer(
                  onPressed: (){

                  },
                  containerText: "Fiction".toUpperCase(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


