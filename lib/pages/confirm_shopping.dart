import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';
import 'package:pustok/utils/custom_toast.dart';

class ConfirmShopping extends StatefulWidget {
  const ConfirmShopping({Key? key}) : super(key: key);

  @override
  State<ConfirmShopping> createState() => _ConfirmShoppingState();
}

class _ConfirmShoppingState extends State<ConfirmShopping> {
  @override
  Widget build(BuildContext context) {
    return Consumer<PustokProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          appBar: AppWidget(
            leadingIcon: FontAwesomeIcons.backward,
            pageTitle: "Confirm Shopping".toUpperCase(),
            onPressed: (){
              provider.confirmedBuyBookList.clear();
              Navigator.pop(context);
            },
          ),
          body: DecorationContainer(
            imageLocation: "assets/images/confirm_shopping_purchase.jpg",
            imageOpacity: 0.1,
            backgroundColor: kBackgroundColor,
            decorationChild: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                    ),
                    onPressed: (){

                    },
                    child: Ink(
                      width: 270,
                    child: ListTile(
                      horizontalTitleGap: 30,
                      leading: Image.asset("assets/icons/bkash_logo.png",
                          width: 60, height: 60),
                      title: Text(
                        "bKash",
                        style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "acme",
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                  ),
                  onPressed: (){

                  },
                  child: Ink(
                    width: 270,
                    child: ListTile(
                      horizontalTitleGap: 35,
                      contentPadding: EdgeInsets.only(left: 30),

                      leading: Image.asset("assets/icons/nagad_logo.png",
                          width: 40, height: 40),
                      title: Text(
                        "Nagad",
                        style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "acme",
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                  ),
                  onPressed: (){

                  },
                  child: Ink(
                    width: 270,
                    child: ListTile(
                      horizontalTitleGap: 30,
                      leading: Image.asset("assets/icons/pay_pal_logo.png",
                          width: 60, height: 60),
                      title: Text(
                        "PayPaL",
                        style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "acme",
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                  ),
                  onPressed: (){
                      provider.myPurchasedBookList.add(provider.confirmedBuyBookList[0]);
                      provider.addShopList.remove(provider.confirmedBuyBookList[0]);
                      provider.confirmedBuyBookList.clear();
                      CustomToast.toastShower("Successfully Purchased", Colors.green);
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return HomePage();
                      })
                      );
                  },
                  child: Ink(
                    width: 270,
                    child: const ListTile(
                      contentPadding:  EdgeInsets.only(left:30),
                      horizontalTitleGap: 10,
                      leading: Icon(Icons.shopping_basket,
                      size: 30,
                      color: Colors.red,),
                      title: Text(
                        "Cash On Delivary",
                        style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "acme",
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),

              ],

            ),
          ),
        );
      }
    );
  }
}
