import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/drawer_item.dart';
import 'package:pustok/pages/user_profile_page.dart';
import 'package:pustok/pages/welcome_page.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/custom_toast.dart';
import 'package:pustok/utils/database_constants.dart';
import 'package:pustok/utils/memory_management.dart';
class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_,provider,___) {
        return Container(
          height:double.infinity,
          width: 300,
          child: Scaffold(
            backgroundColor: Colors.lightBlueAccent.shade700,
            body: Consumer<PustokProvider>(
              builder: (_,pustokProvider,___) {
                return SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                       Padding(
                        padding: EdgeInsets.only(top:30.0,left:28),
                        child:  Image.asset("assets/icons/pustok_icon.png"),
                      ),
                      SizedBox(
                        height:40,
                      ),
                      DrawerItem(
                        drawerText: "Profile".toUpperCase(),
                        drawerIcon: FontAwesomeIcons.user,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context){
                              return UserProfilePage();
                            })
                          );
                        },
                      ),
                      SizedBox(
                        height:30,
                      ),
                      DrawerItem(
                        drawerText: "Developers Info".toUpperCase(),
                        drawerIcon: FontAwesomeIcons.info,
                        onPressed: () {

                        },
                      ),
                      SizedBox(
                        height:30,
                      ),
                      DrawerItem(
                        drawerText: "Delete Account".toUpperCase(),
                        drawerIcon: FontAwesomeIcons.cross,
                        onPressed: () async{
                          bool value = await provider.deleteUserAccount(DatabaseConstants.USER_TABLE_NAME);
                          if(value == true){
                            pustokProvider.addShopList.clear();
                            pustokProvider.confirmedBuyBookList.clear();
                            pustokProvider.myPurchasedBookList.clear();
                            CustomToast.toastShower("Account Successfully Deleted", Colors.green);
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return WelcomePage();
                            })
                            );
                          }else{
                            CustomToast.toastShower("Something is wrong, Check Again", Colors.red);
                          }


                        },
                      ),
                      SizedBox(
                        height:30,
                      ),
                      DrawerItem(
                        drawerText: "Log Out".toUpperCase(),
                        drawerIcon: FontAwesomeIcons.backward,
                        onPressed: (){
                          pustokProvider.addShopList.clear();
                          pustokProvider.confirmedBuyBookList.clear();
                          pustokProvider.myPurchasedBookList.clear();
                           MemoryManagement.setLoggedIn(isLogged: false);
                           provider.users.clear();
                           Navigator.push(context, MaterialPageRoute(builder: (context) {
                             return WelcomePage();
                           })
                           );
                        },
                      ),

                    ],
                  ),
                );
              }
            ),
          ),
        );
      }
    );
  }
}
