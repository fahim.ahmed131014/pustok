import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';

class FavouritePage extends StatefulWidget {


  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {



  @override
  Widget build(BuildContext context) {
    return Consumer<PustokProvider>(
        builder: (_,provider,___) {
          return Scaffold(
            appBar: AppWidget(
              leadingIcon : FontAwesomeIcons.backward,
              pageTitle: "Favourites".toUpperCase(),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            body: SafeArea(
              child: DecorationContainer(
                imageLocation: "assets/images/add_to_cart_background.jpg",
                backgroundColor: kBackgroundColor,
                imageOpacity: 0.2,
                decorationChild:  SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: 20,
                      ),

                      provider.favouriteBookList.isEmpty? Padding(
                        padding: const EdgeInsets.only(top: 300.0),
                        child: Center(
                            child: Text(
                              "Nothing to Buy, Add Some Books First",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.lightBlueAccent,
                                  fontFamily: "acme"
                              ),
                            )
                        ),
                      ):
                      ListView.separated(
                          itemCount: provider.favouriteBookList.length,
                          separatorBuilder: (context,index){
                            return SizedBox(
                              height: 10,
                            );
                          },
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Container(
                              height: 300,
                              width: kBoxContainerWidth,
                              color: kTransparentColor,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(provider.favouriteBookList[index].imageLocation!,
                                      height: 150,
                                      width: 100,
                                    ),
                                    SizedBox(
                                      height: kBookContainerSizedBoxHeight,
                                    ),
                                    Text(provider.favouriteBookList[index].bookName!.toUpperCase(),
                                        style: kBookNameTextStyle),
                                    SizedBox(
                                      height: kBookContainerSizedBoxHeight,
                                    ),
                                    Text(provider.favouriteBookList[index].bookWriter!.toUpperCase(),
                                        style: kBookWriterTextStyle),
                                    SizedBox(
                                      height: 3,
                                    ),
                                    ElevatedButton(
                                      onPressed: (){
                                           provider.addShopList.add(provider.favouriteBookList[index]);
                                           provider.favouriteBookList.remove(provider.favouriteBookList[index]);
                                      },
                                      child: Text("Add To Cart".toUpperCase(),
                                          style:TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "acme",
                                          )
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    ],
                  ),
                ),


              ),
            ),

          );
        }
    );
  }
}
