import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/common_widget/home_container.dart';
import 'package:pustok/pages/category_page.dart';
import 'package:pustok/pages/drawer_page.dart';
import 'package:pustok/pages/favourites_page.dart';
import 'package:pustok/pages/my_books.dart';
import 'package:pustok/pages/top_chart.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';

import 'add_cart.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    return Consumer<PustokProvider>(
        builder: (_,provider,___) {
        return Scaffold(
          key: scaffoldKey,
          drawer: DrawerPage(),
          appBar: AppBar(
            leading: IconButton(
              onPressed: (){
                scaffoldKey.currentState!.openDrawer();
              },
              icon: Icon(Icons.view_module,
                color: Colors.lightBlue,
              ),
            ),
            title: Text("Home".toUpperCase(), style: kAppBarTextStyle),
            centerTitle: true,
            backgroundColor: kBlackColor,
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return AddCart();
                  }));
                },
                color:kBlackColor,
                icon: Icon(FontAwesomeIcons.shoppingCart,
                  color: kAppBarColor,),
              ),
            ],


          ),
          body: SafeArea(
            child:DecorationContainer(
              imageLocation: "assets/images/home_page_background.jpg",
              backgroundColor:kBackgroundColor,
              imageOpacity: kImageOpacity,
              decorationChild: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: kHomePageSizedBoxHeight,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: HomeContainer(
                            onPressed: (){
                              Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return CategoryPage();
                                    }));

                            },
                            icon: FontAwesomeIcons.addressBook,
                            containerText: "Category".toUpperCase(),
                          ),
                        ),

                        Expanded(
                          child: HomeContainer(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context){
                                  return TopChart();
                                }),
                                );

                            },
                            icon: FontAwesomeIcons.award,
                            containerText: "TOP CHART".toUpperCase(),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: kHomePageSizedBoxHeight,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: HomeContainer(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context){
                                return FavouritePage();
                              }),
                              );
                            },
                            icon: FontAwesomeIcons.bookmark,
                            containerText: "Favourites".toUpperCase(),
                          ),
                        ),
                        Expanded(
                          child: HomeContainer(
                            onPressed: (){
                               Navigator.push(context, MaterialPageRoute(builder: (context){
                                 return MyBooks();
                               }),
                               );
                            },
                            icon: FontAwesomeIcons.bookReader,
                            containerText: "My Library".toUpperCase(),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: kHomePageSizedBoxHeight,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: HomeContainer(
                            onPressed: (){

                            },
                            icon: FontAwesomeIcons.book,
                            containerText: "Free Books".toUpperCase(),
                          ),
                        ),
                        Expanded(
                          child: HomeContainer(
                            onPressed: (){

                            },
                            icon: FontAwesomeIcons.newspaper,
                            containerText: "Latest Books".toUpperCase(),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: kHomePageSizedBoxHeight,
                    ),




                  ],
                ),
              ),
            ),
          // body: Column(
          //   mainAxisAlignment: MainAxisAlignment.start,
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     SizedBox(
          //       height:25,
          //     ),
          //     Container(
          //       margin: EdgeInsets.only(left:30,right:30),
          //       padding: EdgeInsets.only(left:10,right:10),
          //       decoration: BoxDecoration(
          //           color: Colors.black38,
          //           borderRadius: BorderRadius.circular(10)
          //       ),
          //       child: TextField(
          //         decoration: InputDecoration(
          //           border: InputBorder.none,
          //           prefixIcon: Icon(FontAwesomeIcons.search),
          //           hintText: "What are you looking for?",
          //           hintStyle:
          //           TextStyle(color: Colors.white, fontSize: 15),
          //           suffixIcon: Icon(Icons.menu),
          //
          //         ),
          //       ),
          //     ),
          //     SizedBox(
          //       height:25,
          //     ),
          //     Padding(
          //       padding: const EdgeInsets.only(left:30),
          //       child: Text("Explore",
          //       style: TextStyle(
          //         fontSize: 30,
          //         fontWeight: FontWeight.bold,
          //         color: Colors.white,
          //         fontFamily: "acme",
          //       ),),
          //     ),
          //     SizedBox(
          //       height: 20,
          //     ),
          //     Padding(
          //       padding: const EdgeInsets.only(left:30.0),
          //       child: Container(
          //         height: 180,
          //         width: 370,
          //         decoration: BoxDecoration(
          //           color: Colors.black38,
          //           borderRadius: BorderRadius.circular(10),
          //       ),
          //         child: Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Padding(
          //               padding: const EdgeInsets.only(left:10,top:10),
          //               child: Text("Categories",style: TextStyle(
          //                 fontSize: 18,
          //                 fontWeight: FontWeight.bold,
          //                 fontFamily: "acme"
          //               ),),
          //             ),
          //
          //           ],
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            unselectedItemColor: Colors.white,
            selectedItemColor: Colors.blue,
            currentIndex: provider.bottomNavigationIndex,
            onTap: (index) => provider.bottomNavigationIndex = index,
            items: [
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.home),
                label: "Home"
              ),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.bookReader),
                  label: "My Library"
              ),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.heart),
                  label: "Favourites"
              ),
            ],
          ),
          );
      }
    );
  }
}



