import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pustok/common_widget/decoration_container.dart';
import 'package:pustok/pages/login_page.dart';
import 'package:pustok/pages/registration_page.dart';
import 'package:pustok/utils/constants.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: DecorationContainer(
          imageLocation: "assets/images/welcome_page_background.jpg",
          backgroundColor: kBackgroundColor,
          imageOpacity: kImageOpacity,
          decorationChild: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:200.0),
                  child: Text(
                    "Welcome!",
                  style: TextStyle(
                    fontSize: 62,
                    fontWeight: FontWeight.bold,
                    fontFamily: "acme",
                    ),
                  ),
                ),
                SizedBox(
                  height:2,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 65.0),
                  child: Text("Read, Learn And Teach",
                  style:TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 18,

                    ),
                  ),
                ),
                SizedBox(
                  height:100,
                ),
                Container(
                  height:55,
                  width: 280,
                  child: ElevatedButton.icon(
                      onPressed:(){
                        Navigator.push(context, MaterialPageRoute(builder: (context){
                          return LoginPage();
                        })
                        );
                      },
                      icon: Icon(FontAwesomeIcons.signInAlt),
                      label: Text("Login".toUpperCase(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: "acme",
                      ),),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.blueAccent.shade700,
                      ),
                  ),
                ),
                SizedBox(
                  height:30,
                ),
                Container(
                  height:55,
                  width: 280,
                  child: ElevatedButton.icon(
                    onPressed:(){
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return RegistrationPage();
                      })
                      );
                    },
                    icon: Icon(Icons.app_registration),
                    label: Text("SignUp".toUpperCase(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: "acme",
                      ),),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blue.shade600,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

    );
  }
}
