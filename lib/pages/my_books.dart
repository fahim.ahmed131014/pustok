import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pustok/common_widget/app_widget.dart';
import 'package:pustok/common_widget/decoration_container.dart';

import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pages/read_books.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';
import 'package:pustok/utils/constants.dart';

class MyBooks extends StatefulWidget {


  @override
  _MyBooksState createState() => _MyBooksState();
}

class _MyBooksState extends State<MyBooks> {



  @override
  Widget build(BuildContext context) {
    return Consumer<PustokProvider>(
        builder: (_,provider,___) {
          return Scaffold(
            appBar: AppWidget(
              leadingIcon : FontAwesomeIcons.backward,
              pageTitle: "MyBooks".toUpperCase(),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return HomePage();
                })
                );
              },
            ),
            body: SafeArea(
              child: DecorationContainer(
                imageLocation: "assets/images/add_to_cart_background.jpg",
                backgroundColor: kBackgroundColor,
                imageOpacity: 0.2,
                decorationChild:  SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      provider.myPurchasedBookList.isEmpty? Padding(
                        padding: const EdgeInsets.only(top: 300.0),
                        child: Center(
                            child: Text(
                              "No Books To Your Library",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.lightBlueAccent,
                                  fontFamily: "acme"
                              ),
                            )
                        ),
                      ):
                      ListView.separated(
                          itemCount: provider.myPurchasedBookList.length,
                          separatorBuilder: (context,index){
                            return SizedBox(
                              height: 10,
                            );
                          },
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Container(
                              height: 300,
                              width: kBoxContainerWidth,
                              color: kTransparentColor,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(provider.myPurchasedBookList[index].imageLocation!,
                                      height: 150,
                                      width: 100,
                                    ),
                                    SizedBox(
                                      height: kBookContainerSizedBoxHeight,
                                    ),
                                    Text(provider.myPurchasedBookList[index].bookName!.toUpperCase(),
                                        style: kBookNameTextStyle),
                                    SizedBox(
                                      height: kBookContainerSizedBoxHeight,
                                    ),
                                    Text(provider.myPurchasedBookList[index].bookWriter!.toUpperCase(),
                                        style: kBookWriterTextStyle),
                                    SizedBox(
                                      height: 3,
                                    ),
                                    ElevatedButton(
                                      onPressed: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context){
                                          return ReadBooks(
                                              bookInfo: provider.myPurchasedBookList[index]
                                          );
                                        })
                                        );
                                      },
                                      child: Text("Read Now".toUpperCase(),
                                          style:TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "acme",
                                          )
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    ],
                  ),
                ),


              ),
            ),

          );
        }
    );
  }
}
