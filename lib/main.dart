import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pustok/pages/home_page.dart';
import 'package:pustok/pages/splash_screen.dart';
import 'package:pustok/pustok_provider/authentication_provider.dart';
import 'package:pustok/pustok_provider/pustok_provider.dart';

void main() {
  runApp(
      MultiProvider(providers: [
        ChangeNotifierProvider<PustokProvider>(
            create: (context) => PustokProvider(),
        ),
        ChangeNotifierProvider<AuthenticationProvider>(
          create: (context) => AuthenticationProvider(),
        ),
      ],
      child: Pustok(),
      ),
  );
}


class Pustok extends StatelessWidget {
  const Pustok({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
