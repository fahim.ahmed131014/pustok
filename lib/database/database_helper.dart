import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pustok/models/user.dart';
import 'package:pustok/utils/database_constants.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{
  Future<Database> initDatabase()async {
     Directory directory = await getApplicationDocumentsDirectory();
     final path = join(directory.path, 'pustok.db');
     return openDatabase(
       path,
       version: 2,
       onCreate: (Database db , int version) async {
         await db.execute(
           "CREATE TABLE "+ DatabaseConstants.USER_TABLE_NAME +" (userId INTEGER PRIMARY KEY AUTOINCREMENT , name TEXT, email TEXT, password TEXT)"
               "  "
         );
        }
       );
  }


  //insert - To - Database
 Future<int> saveUser (User user) async {
    final db = await initDatabase();
    int value = await db.insert(DatabaseConstants.USER_TABLE_NAME, user.toMap());
    print(value);
    return value;
  }


  //email_ check
  Future<bool> isUserExist (String email) async{
    final db = await initDatabase();
    final mUser = await db.rawQuery("SELECT * FROM user WHERE email = '$email'");
    List<User> users = List.generate(mUser.length, (index){
      return User(
        userId: mUser[index]['userId'],
        email: mUser[index]['email'],
        name: mUser[index]['name'],
        password: mUser[index]['password'],

      );
    });

    if(users.isNotEmpty){
      return true;
    }
    else{
      return false;
    }

  }




  //user_search_login
  Future<List<User>> loginUser (String email, String password) async{
    final db = await initDatabase();
    final mUser = await db.rawQuery("SELECT * FROM user WHERE email = '$email' and password = '$password' ");
    List<User> users = List.generate(mUser.length, (index){
      return User(
        userId: mUser[index]['userId'],
        email: mUser[index]['email'],
        name: mUser[index]['name'],
        password: mUser[index]['password'],

      );
    });

    return users;
  }



  //DeleteAccount
  Future<int> deleteUserAccount(String tableName)async{
    final db = await initDatabase();
    return db.delete(tableName);
  }





}