import 'package:pustok/database/database_helper.dart';
import 'package:pustok/models/user.dart';

class PustokRepository{

  //insert - User - Onto - UserTable
  Future<int> saveUser(User user) async{
    return DatabaseHelper().saveUser(user);
  }


  //isUserExist in Database
  Future<bool> isUserExist(String email) async{
    return DatabaseHelper().isUserExist(email);
  }


  //LoginUser Info
  Future<List<User>> loginUser(String email, String password) async{
    return DatabaseHelper().loginUser(email, password);
  }

  //DeleteAccount
  Future<int> deleteUserAccount(String tableName)async{
    return DatabaseHelper().deleteUserAccount(tableName);
  }




}