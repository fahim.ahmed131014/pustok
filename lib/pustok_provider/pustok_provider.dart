import 'package:flutter/cupertino.dart';
import 'package:pustok/models/book_info.dart';
import 'package:pustok/models/book_list.dart';

class PustokProvider with ChangeNotifier{
 List<BookInfo>? _addShopList = [];
 final BookList _bookList = BookList();
 List<BookInfo>? _favouriteBookList =[];
 List<BookInfo>? _myPurchasedBookList =[];
 List<BookInfo>? _confirmedBuyBookList = [];
 int? _bottomNavigationIndex = 0;


 List<BookInfo> get addShopList => _addShopList!;

 set addShopList(List<BookInfo> value) {
    _addShopList = value;
    notifyListeners();
  }

 BookList get bookList => _bookList;

 List<BookInfo> get favouriteBookList => _favouriteBookList!;

  set favouriteBookList(List<BookInfo> value) {
    _favouriteBookList = value;
    notifyListeners();
  }

  List<BookInfo> get myPurchasedBookList => _myPurchasedBookList!;
  set myPurchasedBookList(List<BookInfo> value) {
    _myPurchasedBookList = value;
    notifyListeners();
  }

 List<BookInfo> get confirmedBuyBookList => _confirmedBuyBookList!;

  set confirmedBuyBookList(List<BookInfo> value) {
    _confirmedBuyBookList = value;
    notifyListeners();
  }

 int get bottomNavigationIndex => _bottomNavigationIndex!;

  set bottomNavigationIndex(int value) {
    _bottomNavigationIndex = value;
    notifyListeners();
  }
}