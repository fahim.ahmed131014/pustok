import 'package:flutter/cupertino.dart';
import 'package:pustok/models/user.dart';
import 'package:pustok/repository/pustok_repository.dart';

class AuthenticationProvider with ChangeNotifier{
  bool? _isPasswordShowingText = true;
  bool? _isPasswordEyeColorEnabled = false;
  bool? _status = true;
  List<User> _users =[];






  // check on registration whether user is existed or not
  Future<bool> isUserExist(String email)async{
    bool value;
    value = await PustokRepository().isUserExist(email);
    if(value == true){
      status = true;
    }
    else{
      status = false;
    }
    notifyListeners();
    print(status);
    return status;
  }




  // check on user whether user is registered or not
  Future<bool> loginUser(User user)async{
    users = await PustokRepository().loginUser(user.email, user.password);
    if(users.isNotEmpty){
      status = true;
    }
    else{
      status = false;
    }
    notifyListeners();
    print(status);
    return status;
  }




  Future<bool> saveUser(User user) async{
    var value = await PustokRepository().saveUser(user);
    if(value>0)
      {
       status = true;
      }
    else{
      status = false;
    }
    notifyListeners();
    print(status);
    return status;
  }






  bool get isPasswordShowingText => _isPasswordShowingText!;
  set isPasswordShowingText(bool value) {
    _isPasswordShowingText = value;
    notifyListeners();
  }





  bool get isPasswordEyeColorEnabled => _isPasswordEyeColorEnabled!;
  set isPasswordEyeColorEnabled(bool value) {
    _isPasswordEyeColorEnabled = value;
    notifyListeners();
  }




  bool get status => _status!;
  set status(bool value) {
    _status = value;
    notifyListeners();
  }

  Future<bool> deleteUserAccount(String tableName) async{
    int value = await PustokRepository().deleteUserAccount(tableName);
    if(value>0)
    {
      status = true;
    }
    else{
      status = false;
    }
    notifyListeners();
    print(status);
    return status;
  }

  List<User> get users => _users;

  set users(List<User> value) {
    _users = value;
    notifyListeners();
  }
}