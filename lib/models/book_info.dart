class BookInfo{
  String? imageLocation;
  String? bookName;
  String? bookWriter;
  String? bookGenre;
  String? bookPublished;
  String? bookPdfLocation;

  BookInfo(this.imageLocation, this.bookName, this.bookWriter, this.bookGenre, this.bookPublished, this.bookPdfLocation);
}