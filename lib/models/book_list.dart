import 'book_info.dart';

class BookList{
  List<BookInfo> topChartBookStoreList = [
    BookInfo("assets/images/the_da_vinci_code.jpg", "the da vinci code", "dan brown", "action - adventure", "2003", "assets/pdf/the_da_vinci_code.pdf"),
    BookInfo("assets/images/in_search_of_lost_time.jpg", "in search of lost time", "marcel proust", "fiction", "1993", "assets/pdf/in_search_of_lost_time.pdf"),
    BookInfo("assets/images/ulysses.jpg", "ulysses", "james joyce", "fiction", "1920","assets/pdf/ulysses.pdf"),
    BookInfo("assets/images/the_great_gatsby.jpg", "the great gatsby", "scott fitzgerald", "fiction", "1925","assets/pdf/the_great_gatsby.pdf"),
    BookInfo("assets/images/moby_dick.jpg", "moby dick", "hermen melville", "adventure", "1851","assets/pdf/moby_dick.pdf"),
    BookInfo("assets/images/war_and_peace.jpg", "war and peace", "leo tolstoy", "fiction", "1867","assets/pdf/war_and_peace.pdf"),
    BookInfo("assets/images/hamlet.jpg", "hamlet", "william shakespeare", "classic", "1609","assets/pdf/hamlet.pdf"),
    BookInfo("assets/images/the_odyssey.jpg", "the odyssey", "homer", "classic", "1614","assets/pdf/the_odyssey.pdf"),
    BookInfo("assets/images/the_divine_comedy.jpg", "the divide comedy", "dante alighieri", "classic", "1472","assets/pdf/the_divine_comedy.pdf"),
    BookInfo("assets/images/lolita.jpg", "lolita", "vladimir nabokov", "fiction", "1955","assets/pdf/lolita.pdf"),
    BookInfo("assets/images/crime_and_punishment.jpg", "crime and punishment", "fyodor dostoevesky", "fiction", "1866","assets/pdf/crime_and_punishment.pdf"),
  ];

  List<BookInfo> actionBookStoreList = [
    BookInfo("assets/images/treasure_island.jpg", "treasure island", "robert louis stevenson", "action - adventure", "1883","assets/pdf/treasure_island.pdf"),
    BookInfo("assets/images/the_alchemist.jpg", "The Alchemist", "Paulo Coelho", "action - adventure", "1990","assets/pdf/the_alchemist.pdf"),
    BookInfo("assets/images/kim.jpg", "kim", "Rudyard Kipling", "action - adventure", "1901","assets/pdf/kim.pdf"),
    BookInfo("assets/images/the_sentinel.jpg", "The Sentinel", "Lee Child", "action - adventure", "2020","assets/pdf/the_sentinel.pdf"),
    BookInfo("assets/images/the_call_of_the_wild.jpg", "the call of the wild", "jack london", "action - adventure", "1903","assets/pdf/the_call_of_the_wild.pdf"),
    BookInfo("assets/images/the_princess_bride.jpg", "the princess bride", "william goldman", "action - adventure", "1973","assets/pdf/the_princess_bride.pdf"),
    BookInfo("assets/images/on_the_beach.jpg", "On The Beach", "Nevil Shute", "action - adventure", "2009","assets/pdf/on_the_beach.pdf"),
    BookInfo("assets/images/the_three_musketeers.jpg", "The Three Musketeers", "alexandre dumas", "action - adventure", "1844","assets/pdf/the_three_musketeers.pdf"),
    BookInfo("assets/images/ivanhoe.jpg", "Ivanhoe", "Walter Scott", "action - adventure", "1819","assets/pdf/ivanhoe.pdf"),
    BookInfo("assets/images/hatchet.jpg", "Hatchet", "Gary Paulsen", "action - adventure", "1986","assets/pdf/hatchet.pdf"),
    BookInfo("assets/images/the_riddle_of_the_sands.jpg", "The Riddle of the Sands", "Erskine Childers", "action - adventure", "1903","assets/pdf/the_riddle_of_the_sands.pdf"),
  ];

  List<BookInfo> detectiveBookStoreList = [
    BookInfo("assets/images/gone_girl.jpg", "gone girl", "Gillian Flynn", "detective - mystery", "2012","assets/pdf/gone_girl.pdf"),
    BookInfo("assets/images/death_on_the_nile.jpg", "death on the nile", "agatha christie", "detective - mystery", "1937","assets/pdf/death_on_the_nile.pdf"),
    BookInfo("assets/images/in_the_woods.jpg", "in the Woods", "Tana French", "detective - mystery", "2007","assets/pdf/in_the_woods.pdf"),
    BookInfo("assets/images/the_big_sleep.jpg", "The Big Sleep", "raymond chandler", "detective - mystery", "1939","assets/pdf/the_big_sleep.pdf"),
    BookInfo("assets/images/the_moonstone.jpg", "The Moonstone", "Wilkie Collins", "detective - mystery", "1868","assets/pdf/moonstone.pdf"),
    BookInfo("assets/images/maisie_dobbs.jpg", "Maisie Dobbs", "Jacqueline Winspear", "detective - mystery", "2003","assets/pdf/maisie_doobs.pdf"),
    BookInfo("assets/images/the_big_four.jpg", "The Big Four", "Agatha Christie", "detective - mystery", "1927","assets/pdf/the_big_four.pdf"),
    BookInfo("assets/images/the_guest_list.jpg", "The Guest List", "Lucy Foley", "detective - mystery", "2020","assets/pdf/the_guest_list.pdf"),
    BookInfo("assets/images/whose_body.jpg", "Whose Body?", "Dorothy L. Sayers", "detective - mystery", "1923","assets/pdf/whose_body.pdf"),
    BookInfo("assets/images/still_life.jpg", "Still Life", "Louise Penny", "detective - mystery", "2005","assets/pdf/still_life.pdf"),
    BookInfo("assets/images/the_moving_finger.jpg", "The Moving Finger", "Agatha Christie", "detective - mystery", "1942","assets/pdf/the_moving_finger.pdf"),
  ];





}