class User{
  dynamic userId;
  dynamic name;
  dynamic email;
  dynamic password;


  User({this.userId, this.email, this.password, this.name});

  Map<String,dynamic> toMap(){
    return <String,dynamic>{
      "userId": userId,
      "name": name,
      "email" : email,
      "password" : password,
    };
  }



}