import 'package:flutter/material.dart';

//App Name Constant
const kAppName= "Pustok";


//Home Page Constants
const kHomePageContainerTextStyle = TextStyle(fontSize: 12, fontWeight: FontWeight.bold,fontFamily: "acme");
const kHomeContainerSizedBoxHeight = 16.0;
const kHomePageIconSize = 50.0;
const kHomePageSizedBoxHeight = 30.0;


//Category Page Constants
const kCategoryPageTextStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.bold, fontFamily: "acme",color: Colors.lightBlueAccent);




//AppBar Constants
const kAppBarTextStyle = TextStyle(color: kAppBarColor, fontWeight: FontWeight.bold, fontFamily: "acme");
const kAddToCartTextStyle = TextStyle(fontSize: 12, color: kAppBarColor,fontFamily: "acme");
const kCategoryContainerHeight = 60.0;
const kMarginValue = 20.0;
const kCategoryContainerBorderRadius = 10.0;






//Color Constants
const kBackgroundColor = Colors.white54;
const kBlackColor = Colors.black;
const kAppBarColor = Colors.lightBlueAccent;
const kTransparentColor = Colors.transparent;



//ImageOpacity
const kImageOpacity = 0.4;


// Column Main Axis Alignment Constants
const kColumnMainAxis = MainAxisAlignment.center;



//Book Button Constants
const kButtonMarginValue = 6.0;
const kBookButtonTextStyle = TextStyle(fontSize: 15, fontWeight: FontWeight.bold);
const kBookButtonPaddingValue =  8.0;
const kBoxButtonSizedBoxValue = 22.0;




//Book Container Widget
const kBoxContainerHeight = 200.0;
const kBoxContainerWidth = 400.0;
const kBookNameTextStyle = TextStyle(fontFamily: "acme", fontSize: 20, color: Colors.white);
const kBookWriterTextStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.normal, fontFamily: "acme");
const kBookGenreTextStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 18);
const kBookContainerSizedBoxHeight = 3.0;
const kBookContainerSizedBoxHeight2 = 8.0;