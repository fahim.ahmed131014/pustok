import 'package:shared_preferences/shared_preferences.dart';

class MemoryManagement {
  static final String IS_LOGGED = "logged-in";
  static SharedPreferences? prefs;

  static Future<bool> initSharedPreferences()async{
     prefs = await SharedPreferences.getInstance();
     return true;
  }


  static void setLoggedIn({required bool isLogged}){
     prefs!.setBool(IS_LOGGED, isLogged);
  }
  static bool getLoggedIn(){
    bool? value = prefs!.getBool(IS_LOGGED);
    return value!;
  }

}